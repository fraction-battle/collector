CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE SCHEMA IF NOT EXISTS collector;


CREATE TABLE IF NOT EXISTS collector.rank
(
    id                 uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    name               varchar NOT NULL,
    significance_order int     NOT NULL
);

COMMENT ON COLUMN collector.rank.significance_order IS 'higher is better. Starts with 1';

INSERT INTO collector.rank (name, significance_order)
VALUES ('noob', 1),
       ('ok', 2),
       ('gosu', 3);

CREATE TABLE collector.users
(
    id      uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    rank_id uuid NOT NULL REFERENCES collector.rank (id)
);
