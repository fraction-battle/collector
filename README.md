# Fraction battle match info collector module

## Project installation

### Requirements

1. Postgresql server 13
2. Java 16+
3. Import default-code-style (settings -> code style -> setting_hexagon -> import IDEA xml)
4. You should run app inside docker container separated from another apps
